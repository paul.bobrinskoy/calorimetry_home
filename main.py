"""
Praktikum Digitalisierung

Kalorimetrie - Küchentischversuch

Template Code:
Use this file to create your measurement code

@author: Paul Bobrinskoy

"""

from functions import m_json
from functions import m_pck

path_datasheets_folder = "datasheets"

# Mit entweder "heat_capacity" oder "newton" Experiment.
# Kommt drauf an welches Experiment wir durchführen wollen
experiment = 'heat_capacity'   # heat_capacity or newton
while experiment not in ['heat_capacity', 'newton']:
    experiment = input('type "heat_capacity" or "newton", depending what experiment you want to test')
    
path_setup = "datasheets/setup_"+ experiment +".json"
path_data_folder = 'data/measured_data_'+ experiment


metadata = m_json.get_metadata_from_setup(path_setup)
m_json.add_temperature_sensor_serials(path_datasheets_folder, metadata)
measured_data = m_pck.get_meas_data_calorimetry(metadata)
m_pck.logging_calorimetry(measured_data, metadata, path_data_folder, path_datasheets_folder)
m_json.archiv_json(path_datasheets_folder, path_setup, path_data_folder)